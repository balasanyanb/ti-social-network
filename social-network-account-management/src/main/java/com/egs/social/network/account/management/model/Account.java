package com.egs.social.network.account.management.model;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.AbstractPersistable;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(schema = "social_network_account_management")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Account extends AbstractPersistable<Long> {

  @Column(name = "user_id", nullable = false, unique = true)
  private String userId;

  @Column(name = "birthday", nullable = false)
  private LocalDateTime birthday;

  @Column(name = "description", length = 100)
  private String description;

  @Column(name = "first_name", nullable = false, length = 45)
  private String firstName;

  @Column(name = "middle_name", length = 45)
  private String middleName;

  @Column(name = "last_name", nullable = false, length = 45)
  private String lastName;

  @Column(name = "created_at", nullable = false)
  private LocalDateTime createdAt;

  @SuppressFBWarnings("UPM_UNCALLED_PRIVATE_METHOD")
  @PrePersist
  private void onCreate() {
    this.createdAt = LocalDateTime.now();
  }
}
