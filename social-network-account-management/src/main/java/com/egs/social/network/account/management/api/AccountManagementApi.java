package com.egs.social.network.account.management.api;

import com.egs.social.network.common.dto.account.management.AccountDto;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface AccountManagementApi {

  @GetMapping("/account")
  AccountDto getAccount();

  @PutMapping(value = "/account")
  AccountDto updateAccount(@Valid @RequestBody AccountDto accountDTO);

  @PostMapping(value = "/account")
  AccountDto createAccount(@Valid @RequestBody AccountDto accountDTO);
}
