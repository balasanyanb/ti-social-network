package com.egs.social.network.account.management.repository;

import com.egs.social.network.account.management.model.Account;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

  Optional<Account> findByUserId(String userId);
}
