package com.egs.social.network.account.management.mapper;

import com.egs.social.network.account.management.model.Account;
import com.egs.social.network.common.dto.account.management.AccountDto;
import java.util.function.BiFunction;
import org.springframework.stereotype.Component;

/**
 * AccountMapper
 *
 * @author zaruhig
 */
@Component
public class AccountMapper {

  public AccountDto modelToDto(Account account) {
    return AccountDto.builder()
        .lastName(account.getLastName())
        .birthday(account.getBirthday())
        .description(account.getDescription())
        .middleName(account.getMiddleName())
        .firstName(account.getFirstName())
        .build();
  }

  public Account dtoToModel(AccountDto accountDTO) {
    return Account.builder()
        .firstName(accountDTO.getFirstName())
        .lastName(accountDTO.getLastName())
        .description(accountDTO.getDescription())
        .birthday(accountDTO.getBirthday())
        .middleName(accountDTO.getMiddleName())
        .build();
  }

  public void updateAccount(AccountDto accountDTO, Account account) {
    BiFunction<AccountDto, Account, Account> accountBiFunction =
        (dto, model) -> {
          model.setBirthday(dto.getBirthday());
          model.setDescription(dto.getDescription());
          model.setMiddleName(dto.getMiddleName());
          model.setFirstName(dto.getFirstName());
          model.setLastName(dto.getLastName());
          return model;
        };
    accountBiFunction.apply(accountDTO, account);
  }
}
