package com.egs.social.network.account.management.service.impl;

import com.egs.social.network.account.management.mapper.AccountMapper;
import com.egs.social.network.account.management.model.Account;
import com.egs.social.network.account.management.repository.AccountRepository;
import com.egs.social.network.account.management.service.AccountService;
import com.egs.social.network.common.dto.account.management.AccountDto;
import com.egs.social.network.common.entity.ResponseStatus;
import com.egs.social.network.common.exception.ContentNotFoundException;
import com.egs.social.network.common.util.ContextHolder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Service
public class AccountServiceImpl implements AccountService {

  private final AccountRepository accountRepository;
  private final AccountMapper accountMapper;

  @Override
  @Transactional
  public AccountDto createAccount(final AccountDto accountDto) {
    String userId = ContextHolder.getUserId();

    accountRepository.findByUserId(userId);

    Account account = accountMapper.dtoToModel(accountDto);
    account.setUserId(userId);

    account = accountRepository.save(account);

    return accountMapper.modelToDto(account);
  }

  @Override
  @Transactional
  public AccountDto updateAccount(final AccountDto accountDto) {
    Account account =
        accountRepository
            .findByUserId(ContextHolder.getUserId())
            .orElseThrow(
                () -> new ContentNotFoundException(ResponseStatus.ACCOUNT_NOT_FOUND.getMessage()));
    accountMapper.updateAccount(accountDto, account);
    accountRepository.save(account);
    return accountMapper.modelToDto(account);
  }

  @Override
  public AccountDto getAccount() {
    Account account =
        accountRepository
            .findByUserId(ContextHolder.getUserId())
            .orElseThrow(
                () -> new ContentNotFoundException(ResponseStatus.ACCOUNT_NOT_FOUND.getMessage()));
    return accountMapper.modelToDto(account);
  }
}
