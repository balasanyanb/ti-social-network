package com.egs.social.network.account.management.service;

import com.egs.social.network.common.dto.account.management.AccountDto;

public interface AccountService {

  AccountDto getAccount();

  AccountDto updateAccount(AccountDto accountDTO);

  AccountDto createAccount(AccountDto accountDTO);
}
