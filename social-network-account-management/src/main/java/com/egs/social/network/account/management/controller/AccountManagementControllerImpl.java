package com.egs.social.network.account.management.controller;

import com.egs.social.network.account.management.api.AccountManagementApi;
import com.egs.social.network.account.management.service.AccountService;
import com.egs.social.network.common.dto.account.management.AccountDto;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
public class AccountManagementControllerImpl implements AccountManagementApi {

  private final AccountService accountService;

  @Override
  public AccountDto getAccount() {
    return accountService.getAccount();
  }

  @Override
  public AccountDto updateAccount(final @Valid @RequestBody AccountDto accountDTO) {
    return accountService.updateAccount(accountDTO);
  }

  @Override
  public AccountDto createAccount(final @Valid @RequestBody AccountDto accountDTO) {
    return accountService.createAccount(accountDTO);
  }
}
