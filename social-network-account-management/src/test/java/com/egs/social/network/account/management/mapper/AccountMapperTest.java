package com.egs.social.network.account.management.mapper;

import static com.egs.social.network.account.management.TestDataCreator.buildAccount;
import static com.egs.social.network.account.management.TestDataCreator.buildAccountDto;
import static org.assertj.core.api.Assertions.assertThat;

import com.egs.social.network.account.management.model.Account;
import com.egs.social.network.common.dto.account.management.AccountDto;
import org.junit.Test;

public class AccountMapperTest {

  @Test
  public void modelToDto() {
    Account account = buildAccount();
    assertThat(account.getFirstName()).isNotNull();
    assertThat(account.getLastName()).isNotNull();
    assertThat(account.getDescription()).isNotNull();
    assertThat(account.getMiddleName()).isNotNull();
  }

  @Test
  public void dtoToModel() {
    AccountDto accountDto = buildAccountDto();
    assertThat(accountDto.getFirstName()).isNotNull();
    assertThat(accountDto.getLastName()).isNotNull();
    assertThat(accountDto.getDescription()).isNotNull();
    assertThat(accountDto.getMiddleName()).isNotNull();
  }


}
