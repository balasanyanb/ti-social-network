package com.egs.social.network.account.management.service;

import static com.egs.social.network.account.management.TestDataCreator.buildAccount;
import static com.egs.social.network.account.management.TestDataCreator.buildAccountDto;
import static com.egs.social.network.account.management.TestDataCreator.buildOptionalAccount;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.egs.social.network.account.management.mapper.AccountMapper;
import com.egs.social.network.account.management.model.Account;
import com.egs.social.network.account.management.repository.AccountRepository;
import com.egs.social.network.account.management.service.impl.AccountServiceImpl;
import com.egs.social.network.common.dto.account.management.AccountDto;
import com.egs.social.network.common.entity.ResponseStatus;
import com.egs.social.network.common.exception.ContentNotFoundException;
import com.egs.social.network.common.util.ContextHolder;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class AccountServiceImplTest {

  @Mock private AccountRepository accountRepository;

  @Spy private AccountMapper accountMapper;

  @InjectMocks private AccountServiceImpl accountService;

  @Before
  public void initialize() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void shouldCreateAccount() {
    // GIVEN
    final String userId = UUID.randomUUID().toString();
    ContextHolder.setUserId(userId);

    final AccountDto request = buildAccountDto();
    final Account persisted = buildAccount();

    when(accountRepository.save(any(Account.class))).thenReturn(persisted);
    persisted.setUserId(userId);
    // WHEN
    accountService.createAccount(request);
    // THEN
    final ArgumentCaptor<Account> captorForAccount = ArgumentCaptor.forClass(Account.class);
    verify(accountRepository, times(1)).save(captorForAccount.capture());

    final Account savedAccount = captorForAccount.getValue();

    assertThat(savedAccount.getUserId()).isEqualTo(userId);
    assertThat(savedAccount.getFirstName()).isEqualTo(request.getFirstName());
  }

  @Test
  public void shouldUpdateAccountSuccessfully() {
    // GIVEN
    final String userId = UUID.randomUUID().toString();
    ContextHolder.setUserId(userId);
    final AccountDto request = buildAccountDto();
    final Optional<Account> account = buildOptionalAccount();
    account.ifPresent(value -> value.setUserId(userId));
    when(accountRepository.findByUserId(userId)).thenReturn(account);
    // WHEN
    accountService.updateAccount(request);
    ArgumentCaptor<Account> argumentCaptor = ArgumentCaptor.forClass(Account.class);
    verify(accountRepository).save(argumentCaptor.capture());
    // THEN
    final Account fromDb = argumentCaptor.getValue();
    assertThat(fromDb).isNotNull();
    assertThat(fromDb.getUserId()).isEqualTo(userId);
    assertThat(fromDb.getFirstName()).isEqualTo(request.getFirstName());
  }

  @Test
  public void shouldNotUpdateAccountWhichNotFound() {
    // GIVEN
    final String userId = UUID.randomUUID().toString();
    ContextHolder.setUserId(userId);

    when(accountRepository.findByUserId(ContextHolder.getUserId())).thenReturn(Optional.empty());
    // WHEN
    final Throwable t = catchThrowable(() -> accountService.updateAccount(any(AccountDto.class)));
    // THEN
    assertThat(t).isNotNull();
    assertThat(t instanceof ContentNotFoundException).isTrue();
    assertThat(t.getMessage()).isEqualToIgnoringCase(ResponseStatus.ACCOUNT_NOT_FOUND.getMessage());
  }

  @Test
  public void shouldGetAccount() {
    // GIVEN
    final String userId = UUID.randomUUID().toString();
    ContextHolder.setUserId(userId);
    when(accountRepository.findByUserId(userId)).thenReturn(Optional.of(new Account()));
    // WHEN
    final AccountDto account = accountService.getAccount();
    // THEN
    verify(accountRepository).findByUserId(userId);
    assertThat(account).isNotNull();
  }

  @Test
  public void shouldNotGetAccount() {
    // GIVEN
    final String userId = UUID.randomUUID().toString();
    ContextHolder.setUserId(userId);
    when(accountRepository.findByUserId(ContextHolder.getUserId()))
        .thenThrow(new ContentNotFoundException(ResponseStatus.ACCOUNT_NOT_FOUND.getMessage()));
    // WHEN
    final Throwable throwable = catchThrowable(() -> accountService.getAccount());
    // THEN
    assertThat(throwable.getMessage()).isNotNull();
    assertThat(throwable.getMessage()).isEqualTo(ResponseStatus.ACCOUNT_NOT_FOUND.getMessage());
  }

  @Test
  public void shouldThrowAccountNotFoundException() {
    String userId = UUID.randomUUID().toString();
    ContextHolder.setUserId(userId);
    when(accountRepository.findByUserId(ContextHolder.getUserId())).thenReturn(Optional.empty());

    Throwable t = catchThrowable(() -> accountService.getAccount());

    assertThat(t).isNotNull();
    assertThat(t.getMessage()).isEqualTo(ResponseStatus.ACCOUNT_NOT_FOUND.getMessage());
  }
}
