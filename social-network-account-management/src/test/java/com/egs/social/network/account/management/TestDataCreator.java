package com.egs.social.network.account.management;

import com.egs.social.network.account.management.model.Account;
import com.egs.social.network.common.dto.account.management.AccountDto;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;
import lombok.experimental.UtilityClass;

@UtilityClass
public class TestDataCreator {

  public static AccountDto buildAccountDto() {
    return AccountDto.builder()
        .firstName("testFirstName")
        .lastName("testLastName")
        .middleName("testMiddleName")
        .birthday(LocalDateTime.of(1994, 5, 28, 1, 1))
        .description("Some description for testing")
        .build();
  }

  public static Optional<Account> buildOptionalAccount() {
    final Account.AccountBuilder builder = Account.builder();
    builder
        .firstName("testFirstName")
        .lastName("testLastName")
        .middleName("testMiddleName")
        .birthday(LocalDateTime.of(1994, 5, 28, 1, 1))
        .description("Some description for testing");
    return Optional.of(builder.build());
  }

  public static Account buildAccount() {
    return Account.builder()
        .userId(UUID.randomUUID().toString())
        .birthday(LocalDateTime.now().minusWeeks(15))
        .firstName("testFirstName")
        .lastName("testLastName")
        .middleName("testMiddleName")
        .description("Some description for testing")
        .createdAt(LocalDateTime.of(1994, 5, 28, 1, 1))
        .build();
  }
}
