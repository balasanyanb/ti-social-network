package com.egs.social.network.account.management.api;

import static com.egs.social.network.account.management.TestDataCreator.buildAccount;
import static com.egs.social.network.account.management.TestDataCreator.buildAccountDto;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.egs.social.network.account.management.controller.AccountManagementControllerImpl;
import com.egs.social.network.account.management.mapper.AccountMapper;
import com.egs.social.network.account.management.model.Account;
import com.egs.social.network.account.management.service.AccountService;
import com.egs.social.network.common.dto.account.management.AccountDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

@Slf4j
public class AccountManagementControllerImplTest {

  @InjectMocks AccountManagementControllerImpl accountManagementController;
  @Mock private AccountService accountService;
  @Spy private AccountMapper accountMapper;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void getAccountShouldReturnAccountDto() {
    // GIVEN
    final AccountDto value = buildAccountDto();
    when(accountService.getAccount()).thenReturn(value);
    // WHEN
    accountManagementController.getAccount();
    // THEN
    assertThat(value).isNotNull();
    assertThat(value.getFirstName()).isEqualTo(buildAccountDto().getFirstName());
  }

  @Test
  public void updateAccountShouldReturnAccountDto() {
    // GIVEN
    final AccountDto accountRequest = buildAccountDto();
    Account value = buildAccount();
    when(accountService.updateAccount(accountRequest)).thenReturn(accountRequest);
    // WHEN
    accountManagementController.updateAccount(accountRequest);
    // THEN
    assertThat(accountRequest).isNotNull();
    assertThat(accountRequest.getFirstName()).isEqualTo(value.getFirstName());
    assertThat(accountRequest.getLastName()).isEqualTo(value.getLastName());
  }

  @Test
  public void createAccountShouldReturnAccountDto() {
    // GIVEN
    final AccountDto accountRequest = buildAccountDto();
    Account value = buildAccount();
    when(accountService.createAccount(accountRequest)).thenReturn(accountRequest);
    // WHEN
    accountManagementController.createAccount(accountRequest);
    // THEN
    assertThat(accountRequest).isNotNull();
    assertThat(accountRequest.getFirstName()).isEqualTo(value.getFirstName());
    assertThat(accountRequest.getLastName()).isEqualTo(value.getLastName());
  }
}
