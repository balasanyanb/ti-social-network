package com.egs.social.network.auth.socialnetworkauth.scenario;

import com.egs.social.network.auth.SocialNetworkAuthApplication;
import com.egs.social.network.auth.socialnetworkauth.DTOHelper;
import com.egs.social.network.auth.socialnetworkauth.URLUtil;
import com.egs.social.network.common.dto.auth.SignUpRequest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SocialNetworkAuthApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InvalidSignUpTest {

    private static HttpHeaders headers = new HttpHeaders();
    @LocalServerPort

    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeClass
    public static void init() {
        headers.setContentType(MediaType.APPLICATION_JSON);
    }

    @Test
    public void testSignUpWithInvalidEmail() {
        SignUpRequest request = DTOHelper.generateValidSignUpRequest();
        request.setEmail("test");
        HttpEntity<SignUpRequest> entity = new HttpEntity<>(request, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                URLUtil.createURL(port, URLUtil.SIGN_UP),
                HttpMethod.POST, entity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testSignUpWithInvalidUserName() {
        SignUpRequest request = DTOHelper.generateValidSignUpRequest();
        request.setUserName(" ");
        HttpEntity<SignUpRequest> entity = new HttpEntity<>(request, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                URLUtil.createURL(port, URLUtil.SIGN_UP),
                HttpMethod.POST, entity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
}

    @Test
    public void testSignUpWithInvalidPassword() {
        SignUpRequest request = DTOHelper.generateValidSignUpRequest();
        request.setPassword("test");
        HttpEntity<SignUpRequest> entity = new HttpEntity<>(request, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                URLUtil.createURL(port, URLUtil.SIGN_UP),
                HttpMethod.POST, entity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
}
