package com.egs.social.network.auth.socialnetworkauth;

import com.egs.social.network.auth.model.User;

import java.time.LocalDateTime;

public class ModelHelper {
    public static User generateValidUser() {
        User user = new User();
        user.setEmail(System.currentTimeMillis() + "@mailinator.com");
        user.setUsername("" + System.currentTimeMillis());
        user.setPassword("12345678");
        user.setCreatedAt(LocalDateTime.now());
        return user;
    }
}
