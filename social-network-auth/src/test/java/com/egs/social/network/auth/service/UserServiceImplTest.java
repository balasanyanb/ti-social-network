package com.egs.social.network.auth.service;

import static com.egs.social.network.auth.UserDataCreator.apply;
import static com.egs.social.network.auth.UserDataCreator.buildSignUpRequest;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.catchThrowable;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.egs.social.network.auth.mapper.SignUpRequestMapper;
import com.egs.social.network.auth.model.User;
import com.egs.social.network.auth.repository.UserRepository;
import com.egs.social.network.common.dto.auth.SignUpRequest;
import com.egs.social.network.common.dto.auth.SignUpResponse;
import com.egs.social.network.common.entity.ResponseStatus;
import java.util.Optional;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

@Slf4j
public class UserServiceImplTest {

  @Mock private SignUpRequestMapper signUpRequestMapper;

  @Mock private PasswordEncoder passwordEncoder;

  @Mock private UserRepository userRepository;

  @InjectMocks private UserServiceImpl userService;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void addUserShouldReturnTrue() {
    // GIVEN
    SignUpRequest signUpRequest = buildSignUpRequest();
    final User user = apply(signUpRequest);

    user.setPassword(signUpRequest.getPassword());
    user.setExternalId(UUID.randomUUID().toString());

    when(signUpRequestMapper.apply(signUpRequest)).thenReturn(user);
    when(passwordEncoder.encode(user.getPassword())).thenReturn(user.getPassword());
    when(userRepository.save(user)).thenReturn(user);
    // WHEN
    final SignUpResponse signUpResponse = userService.addUser(signUpRequest);
    when(userService.addUser(signUpRequest)).thenReturn(signUpResponse);
    // THEN
    final ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
    verify(userRepository).save(captor.capture());

    final User savedUser = captor.getValue();
    assertThat(signUpRequest.getUserName()).isEqualTo(captor.getValue().getUsername());
    assertThat(savedUser).isNotNull();
    assertThat(savedUser.getUsername()).isEqualTo(signUpRequest.getUserName());
    assertThat(signUpResponse).isNotNull();
  }

  @Test
  public void addUserShouldThrowEmailExistsException() {
    // GIVEN
    final String email = buildSignUpRequest().getEmail();
    when(userRepository.existsByEmail(email)).thenReturn(true);
    // WHEN
    Throwable throwable = catchThrowable(() -> userService.addUser(buildSignUpRequest()));
    // THEN
    assertThat(throwable).isNotNull();
    assertThat(throwable.getMessage()).isEqualTo(ResponseStatus.EMAIL_EXISTS.getMessage());
  }

  @Test
  public void addUserShouldThrowUserNameExistsException() {
    // GIVEN
    final String username = buildSignUpRequest().getUserName();
    when(userRepository.existsByUsername(username)).thenReturn(true);
    // WHEN
    Throwable throwable = catchThrowable(() -> userService.addUser(buildSignUpRequest()));
    // THEN
    assertThat(throwable).isNotNull();
    assertThat(throwable.getMessage()).isEqualTo(ResponseStatus.USER_NAME_EXISTS.getMessage());
  }

  @Test
  public void shouldReturnUserByEmailOrUserName() {
    // GIVEN
    final String userName = buildSignUpRequest().getUserName();
    final String email = buildSignUpRequest().getEmail();
    final User user = new User();
    user.setUsername("arayikh");
    user.setEmail("arayikh@mail.ru");
    when(userRepository.findByEmailOrUsername(userName, userName)).thenReturn(Optional.of(user));
    // WHEN
    userService.findByEmailOrUserName(userName);
    // THEN
    assertThat(user).isNotNull();
    assertThat(user.getEmail()).isEqualTo(email);
    assertThat(user.getUsername()).isEqualTo(user.getUsername());
  }

  @Test
  public void findByEmailOrUserNameShouldThrowUsernameNotFoundException() {
    // GIVEN
    final String userName = buildSignUpRequest().getUserName();
    when(userRepository.findByEmailOrUsername(userName, userName))
        .thenThrow(new UsernameNotFoundException("Account not found for provided " + userName));
    // WHEN
    final Throwable throwable = catchThrowable(() -> userService.findByEmailOrUserName(userName));
    // THEN
    assertThat(throwable).isNotNull();
    assertThat(throwable.getMessage()).isEqualTo("Account not found for provided " + userName);
    assertThat(throwable instanceof UsernameNotFoundException).isTrue();
  }

  @Test
  public void findByEmailOrUserNameShouldThrowNullPointerException() {
    // GIVEN
    final String userName = buildSignUpRequest().getUserName();
    when(userRepository.findByEmailOrUsername(userName, userName))
        .thenThrow(new NullPointerException("Account not found for provided null"));
    // WHEN
    final Throwable throwable = catchThrowable(() -> userService.findByEmailOrUserName(null));
    // THEN
    assertThat(throwable).isNotNull();
    assertThat(throwable.getMessage()).isEqualTo("Account not found for provided null");
  }
}
