package com.egs.social.network.auth.socialnetworkauth.scenario;

import static com.egs.social.network.auth.socialnetworkauth.URLUtil.createURL;
import static org.junit.Assert.assertEquals;

import com.egs.social.network.auth.SocialNetworkAuthApplication;
import com.egs.social.network.auth.socialnetworkauth.DTOHelper;
import com.egs.social.network.auth.socialnetworkauth.URLUtil;
import com.egs.social.network.common.dto.auth.SignInRequest;
import com.egs.social.network.common.dto.auth.SignInResponse;
import com.egs.social.network.common.dto.auth.SignUpRequest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = SocialNetworkAuthApplication.class,
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ValidSignInTest {
  private static HttpHeaders headers = new HttpHeaders();
  @LocalServerPort private int port;
  @Autowired private TestRestTemplate restTemplate;

  @BeforeClass
  public static void init() {
    headers.setContentType(MediaType.APPLICATION_JSON);
  }

  @Test
  public void loginUserWithEmail() {
    SignUpRequest signUpRequest = addUser();
    HttpEntity<SignInRequest> entity =
        new HttpEntity<>(
            new SignInRequest(signUpRequest.getEmail(), signUpRequest.getPassword()), headers);
    ResponseEntity<SignInResponse> response =
        restTemplate.exchange(
            createURL(port, URLUtil.SIGN_IN), HttpMethod.POST, entity, SignInResponse.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
  }

  @Test
  public void loginUserWithUserName() {
    SignUpRequest signUpRequest = addUser();
    HttpEntity<SignInRequest> entity =
        new HttpEntity<>(
            new SignInRequest(signUpRequest.getUserName(), signUpRequest.getPassword()), headers);
    ResponseEntity<SignInResponse> response =
        restTemplate.exchange(
            createURL(port, URLUtil.SIGN_IN), HttpMethod.POST, entity, SignInResponse.class);
    assertEquals(HttpStatus.OK, response.getStatusCode());
  }

  private SignUpRequest addUser() {
    SignUpRequest signUpRequest = DTOHelper.generateValidSignUpRequest();
    HttpEntity<SignUpRequest> signUpRequestHttpEntity = new HttpEntity<>(signUpRequest, headers);
    restTemplate.exchange(
        createURL(port, URLUtil.SIGN_UP), HttpMethod.POST, signUpRequestHttpEntity, String.class);
    return signUpRequest;
  }
}
