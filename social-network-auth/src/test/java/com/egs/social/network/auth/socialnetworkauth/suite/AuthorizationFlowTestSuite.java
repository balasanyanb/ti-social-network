package com.egs.social.network.auth.socialnetworkauth.suite;

import com.egs.social.network.auth.socialnetworkauth.scenario.InvalidSignInTest;
import com.egs.social.network.auth.socialnetworkauth.scenario.InvalidSignUpTest;
import com.egs.social.network.auth.socialnetworkauth.scenario.ValidSignInTest;
import com.egs.social.network.auth.socialnetworkauth.scenario.ValidSignUpTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({InvalidSignInTest.class, InvalidSignUpTest.class, ValidSignUpTest.class, ValidSignInTest.class})
public class AuthorizationFlowTestSuite {
}