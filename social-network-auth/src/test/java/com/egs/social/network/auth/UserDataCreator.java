package com.egs.social.network.auth;

import static com.egs.social.network.common.model.Role.ROLE_CLIENT;


import com.egs.social.network.auth.model.User;
import com.egs.social.network.common.dto.auth.SignUpRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import lombok.experimental.UtilityClass;

@UtilityClass
public final class UserDataCreator {

  public static SignUpRequest buildSignUpRequest() {
    return SignUpRequest.builder()
        .email("arayikh@mail.ru")
        .userName("arayikh")
        .password("arayikh")
        .build();
  }

  /**
   * @param signUpRequest = SignUpRequest
   * @return User.class
   */
  public static User apply(final SignUpRequest signUpRequest) {
    return User.builder()
        .email(signUpRequest.getEmail().toLowerCase(Locale.ENGLISH))
        .username(signUpRequest.getUserName())
        .password(signUpRequest.getPassword())
        .roles(new ArrayList<>(Collections.singleton(ROLE_CLIENT)))
        .build();
  }
}
