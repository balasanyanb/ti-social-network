package com.egs.social.network.auth.socialnetworkauth;

import com.egs.social.network.common.dto.auth.SignInRequest;
import com.egs.social.network.common.dto.auth.SignUpRequest;

public class DTOHelper {
    public static SignUpRequest generateValidSignUpRequest(){
        SignUpRequest request = new SignUpRequest();
        request.setEmail(System.currentTimeMillis()+"@mailinator.com");
        request.setUserName(""+System.currentTimeMillis());
        request.setPassword("12345678");
        return request;
    }

    public static SignInRequest generateValidSignInRequest(){
        SignInRequest request = new SignInRequest();
        request.setLogin(System.currentTimeMillis()+"@mailinator.com");
        request.setPassword("12345678");
        return request;
    }
}
