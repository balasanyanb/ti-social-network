package com.egs.social.network.auth.socialnetworkauth.runner;

import com.egs.social.network.auth.socialnetworkauth.suite.AuthorizationFlowTestSuite;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

@Slf4j
public class AuthorizationFlowTestRunner {
    public static void main(String[] args) {
        log.info("Calling AuthorizationFlowTestRunner");
        Result result = JUnitCore.runClasses(AuthorizationFlowTestSuite.class);
        for (Failure failure : result.getFailures()) {
            log.error(failure.toString());
        }
        log.info(String.valueOf(result.wasSuccessful()));
    }
}