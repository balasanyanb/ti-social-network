package com.egs.social.network.auth.socialnetworkauth.scenario;

import com.egs.social.network.auth.SocialNetworkAuthApplication;
import com.egs.social.network.auth.socialnetworkauth.DTOHelper;
import com.egs.social.network.auth.socialnetworkauth.URLUtil;
import com.egs.social.network.common.dto.auth.SignInRequest;
import com.egs.social.network.common.dto.auth.SignInResponse;
import com.egs.social.network.common.dto.auth.SignUpRequest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import static com.egs.social.network.auth.socialnetworkauth.URLUtil.createURL;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpMethod.POST;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = SocialNetworkAuthApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InvalidSignInTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;
    private static HttpHeaders headers = new HttpHeaders();

    @BeforeClass
    public static void init() {
        headers.setContentType(MediaType.APPLICATION_JSON);
    }

    @Test
    public void testSignInWithInvalidPassword() {
        SignInRequest request = DTOHelper.generateValidSignInRequest();
        request.setPassword("test");
        HttpEntity<SignInRequest> entity = new HttpEntity<>(request, headers);
        ResponseEntity<SignInResponse> response = restTemplate.exchange(createURL(port, URLUtil.SIGN_IN),
                POST, entity, SignInResponse.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testSignInWithInvalidLogin() {
        SignInRequest request = DTOHelper.generateValidSignInRequest();
        request.setLogin(" ");
        HttpEntity<SignInRequest> entity = new HttpEntity<>(request, headers);
        ResponseEntity<SignInResponse> response = restTemplate.exchange(
                createURL(port, URLUtil.SIGN_IN),
                POST, entity, SignInResponse.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testSignInWithWrongPassword() {
        SignUpRequest signUpRequest = addUser();
        SignInRequest signInRequest = new SignInRequest(signUpRequest.getEmail(), signUpRequest.getPassword() + 1);
        HttpEntity<SignInRequest> entity = new HttpEntity<>(signInRequest, headers);
        ResponseEntity<SignInResponse> response = restTemplate.exchange(
                createURL(port, URLUtil.SIGN_IN),
                POST, entity, SignInResponse.class);
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
    }

    @Test
    public void testSignInWithWrongLogin() {
        SignUpRequest signUpRequest = addUser();
        SignInRequest signInRequest = new SignInRequest(signUpRequest.getEmail() + 1, signUpRequest.getPassword());
        HttpEntity<SignInRequest> entity = new HttpEntity<>(signInRequest, headers);
        ResponseEntity<SignInResponse> response = restTemplate.exchange(
                createURL(port, URLUtil.SIGN_IN),
                POST, entity, SignInResponse.class);
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
    }

    private SignUpRequest addUser() {
        SignUpRequest signUpRequest = DTOHelper.generateValidSignUpRequest();
        HttpEntity<SignUpRequest> signUpRequestHttpEntity = new HttpEntity<>(signUpRequest, headers);
        restTemplate.exchange(
                createURL(port, URLUtil.SIGN_UP),
                POST, signUpRequestHttpEntity, String.class);
        return signUpRequest;
    }
}
