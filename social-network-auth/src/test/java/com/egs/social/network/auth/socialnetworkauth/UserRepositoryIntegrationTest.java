package com.egs.social.network.auth.socialnetworkauth;

import static com.egs.social.network.auth.socialnetworkauth.ModelHelper.generateValidUser;
import static org.junit.Assert.assertTrue;

import com.egs.social.network.auth.model.User;
import com.egs.social.network.auth.repository.UserRepository;
import java.util.Locale;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Slf4j
public class UserRepositoryIntegrationTest {

  @Autowired private UserRepository repository;

  @Test
  public void testSaveUser() {
    User user = generateValidUser();
    repository.save(user);
    assertTrue(repository.findById(Objects.requireNonNull(user.getId())).isPresent());
  }

  @Test
  public void testExistsByEmailInIgnoreCase() {
    User user = generateValidUser();
    repository.save(user);
    assertTrue(repository.existsByEmail(user.getEmail().toLowerCase(Locale.ENGLISH)));
  }

  @Test
  public void testExistsByUserNameInIgnoreCase() {
    User user = generateValidUser();
    repository.save(user);
    assertTrue(repository.existsByUsername(user.getUsername().toLowerCase(Locale.ENGLISH)));
  }

  @Test
  public void testFindByEmailInIgnoreCase() {
    User user = generateValidUser();
    repository.save(user);
    assertTrue(
        repository
            .findByEmailOrUsername(user.getEmail().toLowerCase(Locale.ENGLISH), user.getEmail())
            .isPresent());
  }

  @Test
  public void testFindByUserNameInIgnoreCase() {
    User user = generateValidUser();
    repository.save(user);
    assertTrue(
        repository
            .findByEmailOrUsername(
                user.getUsername().toLowerCase(Locale.ENGLISH),
                user.getEmail().toLowerCase(Locale.ENGLISH))
            .isPresent());
  }
}
