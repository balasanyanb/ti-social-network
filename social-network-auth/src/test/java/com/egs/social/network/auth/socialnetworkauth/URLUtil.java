package com.egs.social.network.auth.socialnetworkauth;

public class URLUtil {
    public static final String SIGN_UP = "auth/signup";
    public static final String SIGN_IN = "auth/signin";
    private static final String BASE_PATH = "/api/v1/";
    private static final String LOCAL_HOST = "http://localhost:";

    public static String createURL(int port, String url) {
        return new StringBuilder()
                .append(LOCAL_HOST)
                .append(port)
                .append(BASE_PATH)
                .append(url)
                .toString();
    }
}
