package com.egs.social.network.auth.model;

import com.egs.social.network.common.model.Role;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.AbstractPersistable;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(schema = "social_network")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User extends AbstractPersistable<Long> implements Serializable {

  private static final long serialVersionUID = 8322041481342023441L;

  @Column(name = "external_id", nullable = false)
  private String externalId;

  @Column(nullable = false, unique = true, length = 65)
  private String email;

  @ElementCollection(fetch = FetchType.EAGER)
  private List<Role> roles;

  @Column(name = "user_name", nullable = false, unique = true, length = 45)
  private String username;

  @Column(nullable = false, length = 100)
  private String password;

  @Column(name = "created_at", nullable = false)
  private LocalDateTime createdAt;

  @SuppressFBWarnings("UPM_UNCALLED_PRIVATE_METHOD")
  @PrePersist
  private void onCreate() {
    this.createdAt = LocalDateTime.now();
    this.externalId = UUID.randomUUID().toString();
  }
}
