package com.egs.social.network.auth.security;

import com.egs.social.network.auth.model.User;
import java.util.Collection;
import java.util.Collections;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * UserDetailsImpl
 *
 * @author zaruhig
 */
@AllArgsConstructor
public class UserDetailsImpl implements UserDetails {

  private static final long serialVersionUID = -9007978545294295340L;

  private User user;

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return Collections.<GrantedAuthority>singletonList(
        new SimpleGrantedAuthority(user.getRoles().toString()));
  }

  @Override
  public String getPassword() {
    return user.getPassword();
  }

  @Override
  public String getUsername() {
    return user.getUsername();
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }
}
