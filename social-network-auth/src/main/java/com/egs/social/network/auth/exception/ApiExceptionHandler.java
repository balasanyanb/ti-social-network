package com.egs.social.network.auth.exception;

import com.egs.social.network.common.entity.ErrorResponseMy;
import com.egs.social.network.common.entity.ResponseStatus;
import com.egs.social.network.common.exception.AccountExistsException;
import com.egs.social.network.common.exception.ContentNotFoundException;
import com.egs.social.network.common.exception.ContentRequiredException;
import com.egs.social.network.common.exception.SocialNetworkException;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Slf4j
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
  @ExceptionHandler(AccountExistsException.class)
  protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
    ErrorResponseMy response = new ErrorResponseMy(HttpStatus.CONFLICT, ex.getMessage());
    log.error("Getting AccountExistsException : {}", ex.getMessage());
    return handleExceptionInternal(ex, response, new HttpHeaders(), HttpStatus.CONFLICT, request);
  }

  @ExceptionHandler(BadCredentialsException.class)
  protected ResponseEntity<Object> handleBadCredentials(RuntimeException ex, WebRequest request) {
    ErrorResponseMy response =
        new ErrorResponseMy(HttpStatus.UNAUTHORIZED, ResponseStatus.ACCOUNT_NOT_FOUND.getMessage());
    log.error("Getting BadCredentialsException : {}", ex.getMessage());
    return handleExceptionInternal(
        ex, response, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
  }

  @ExceptionHandler(ContentNotFoundException.class)
  protected ResponseEntity<Object> handleContentNotFound(RuntimeException ex, WebRequest request) {
    ErrorResponseMy response =
        new ErrorResponseMy(HttpStatus.NOT_FOUND, ResponseStatus.CONTENT_NOT_FOUND.getMessage());
    log.error("Getting ContentNotFoundException : {}", ex.getMessage());
    return handleExceptionInternal(ex, response, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
  }

  @ExceptionHandler(ContentRequiredException.class)
  protected ResponseEntity<Object> handleContentRequired(RuntimeException ex, WebRequest request) {
    ErrorResponseMy response =
        new ErrorResponseMy(
            HttpStatus.UNPROCESSABLE_ENTITY, ResponseStatus.ACCOUNT_NOT_FOUND.getMessage());
    log.error("Getting ContentRequiredException : {}", ex.getMessage());
    return handleExceptionInternal(
        ex, response, new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY, request);
  }

  @ExceptionHandler(SocialNetworkException.class)
  public void handleSocialNetworkException(HttpServletResponse response, SocialNetworkException ex)
      throws IOException {
    log.error("Getting SocialNetworkException : {}", ex.getMessage());
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    log.error("Getting MethodArgumentNotValidException : {}", ex.getMessage());
    return new ResponseEntity<>(ex.getBindingResult().getModel(), headers, status);
  }

  @ExceptionHandler(AccessDeniedException.class)
  public void handleAccessDeny(HttpServletResponse response) throws IOException {
    log.error("Getting AccessDeniedException : {}");
    response.sendError(HttpStatus.FORBIDDEN.value(), "Access denied");
  }
}
