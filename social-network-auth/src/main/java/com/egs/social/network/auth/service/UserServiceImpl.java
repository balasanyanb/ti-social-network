package com.egs.social.network.auth.service;

import com.egs.social.network.auth.mapper.SignUpRequestMapper;
import com.egs.social.network.auth.model.User;
import com.egs.social.network.auth.repository.UserRepository;
import com.egs.social.network.common.dto.auth.SignUpRequest;
import com.egs.social.network.common.dto.auth.SignUpResponse;
import com.egs.social.network.common.entity.ResponseStatus;
import com.egs.social.network.common.exception.AccountExistsException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;
  private final SignUpRequestMapper signUpRequestMapper;

  @Override
  @Transactional
  public SignUpResponse addUser(SignUpRequest request) {

    if (userRepository.existsByEmail(request.getEmail())) {
      log.info(
          "Calling addUser for {}, {} and getting duplicate email",
          request.getEmail(),
          request.getUserName());
      throw new AccountExistsException(ResponseStatus.EMAIL_EXISTS.getMessage());
    }

    if (userRepository.existsByUsername(request.getUserName())) {
      log.info(
          "Calling addUser for {}, {} and getting duplicate username",
          request.getEmail(),
          request.getUserName());
      throw new AccountExistsException(ResponseStatus.USER_NAME_EXISTS.getMessage());
    }

    User user = signUpRequestMapper.apply(request);
    user.setPassword(passwordEncoder.encode(request.getPassword()));
    userRepository.save(user);

    return new SignUpResponse(ResponseStatus.ACCOUNT_CREATED.getMessage());
  }

  @Override
  @Transactional(readOnly = true)
  public User findByEmailOrUserName(final String userName) {
    return userRepository
        .findByEmailOrUsername(userName, userName)
        .orElseThrow(
            () -> new UsernameNotFoundException("Account not found for provided " + userName));
  }
}
