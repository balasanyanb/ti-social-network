package com.egs.social.network.auth.config;

import com.egs.social.network.auth.security.JwtAuthenticationEntryPoint;
import com.egs.social.network.auth.security.JwtFilter;
import com.egs.social.network.auth.security.UserDetailsServiceImpl;
import com.egs.social.network.common.security.JwtService;
import javax.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
@EnableWebSecurity
@AllArgsConstructor(onConstructor = @__({@Autowired}))
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  private JwtService jwtService;
  private UserDetailsServiceImpl userDetailsService;
  private final JwtAuthenticationEntryPoint entryPoint;

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public AuthenticationManager customAuthenticationManager() throws Exception {
    return authenticationManager();
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.cors()
        .and()
        .csrf()
        .disable()
        .exceptionHandling()
        .authenticationEntryPoint(entryPoint)
        .and()
        // No session will be created or used by spring security
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers("/", "/resources/**")
        .permitAll()
        .antMatchers("/signup")
        .permitAll()
        .antMatchers("/signin")
        .permitAll()
        .and()
        .exceptionHandling()
        .authenticationEntryPoint(
            (req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED))
        .and()
        .addFilterAfter(
            new JwtFilter(jwtService, userDetailsService),
            UsernamePasswordAuthenticationFilter.class)
        .authorizeRequests()
        .anyRequest()
        .authenticated();
  }

  @Override
  public void configure(WebSecurity web) {
    web.ignoring()
        .antMatchers("/v2/api-docs") //
        .antMatchers("/swagger-resources/**") //
        .antMatchers("/swagger-ui.html") //
        .antMatchers("/configuration/**") //
        .antMatchers("/webjars/**") //
        .antMatchers("/public");
  }
}
