package com.egs.social.network.auth.security;

import com.egs.social.network.auth.model.User;
import com.egs.social.network.auth.repository.UserRepository;
import java.util.Locale;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

  private final UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String login) {
    String userName = login.toLowerCase(Locale.ENGLISH);
    final User user =
        userRepository
            .findByEmailOrUsername(userName, userName)
            .orElseThrow(
                () -> new UsernameNotFoundException("Account not found for provided " + userName));
    return new UserDetailsImpl(user);
  }
}
