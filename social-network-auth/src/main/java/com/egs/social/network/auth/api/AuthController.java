package com.egs.social.network.auth.api;

import com.egs.social.network.common.dto.auth.SignInRequest;
import com.egs.social.network.common.dto.auth.SignInResponse;
import com.egs.social.network.common.dto.auth.SignUpRequest;
import com.egs.social.network.common.dto.auth.SignUpResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface AuthController {
  @PostMapping(value = "/signup")
  ResponseEntity<SignUpResponse> signUp(@RequestBody SignUpRequest request);

  /**
   * API <host>:<port>/api/v1/auth/signin POST raw JSON { user_name: userName, password : 12345678 }
   *
   * @param request the SignInRequest for getting user by provided login and password
   * @return ResponseEntity with HTTP Status code
   */
  @PostMapping(value = "/signin")
  ResponseEntity<SignInResponse> signIn(@RequestBody SignInRequest request);
}
