package com.egs.social.network.auth.repository;

import com.egs.social.network.auth.model.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  boolean existsByEmail(String email);

  boolean existsByUsername(String userName);

  Optional<User> findByEmailOrUsername(String email, String userName);

  User findByUsername(String userName);
}
