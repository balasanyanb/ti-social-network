package com.egs.social.network.auth.security;

import com.egs.social.network.common.security.JwtService;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.filter.GenericFilterBean;

@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class JwtFilter extends GenericFilterBean {

  private JwtService jwtService;
  private UserDetailsServiceImpl userDetailsService;

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
      throws IOException, ServletException {
    String token = jwtService.getTokenFromHeader((HttpServletRequest) request);
    if (token != null && jwtService.isTokenValid(token)) {
      String subject = jwtService.getSubject(token);
      UserDetails userDetails = userDetailsService.loadUserByUsername(subject);
      Authentication auth =
          new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
      SecurityContextHolder.getContext().setAuthentication(auth);
    }
    filterChain.doFilter(request, response);
  }
}
