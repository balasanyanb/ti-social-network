package com.egs.social.network.auth.service;

import com.egs.social.network.auth.model.User;
import com.egs.social.network.common.dto.auth.SignUpRequest;
import com.egs.social.network.common.dto.auth.SignUpResponse;

public interface UserService {

  SignUpResponse addUser(SignUpRequest userRegRequest);

  User findByEmailOrUserName(String login);
}
