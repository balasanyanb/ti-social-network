package com.egs.social.network.auth;

import com.egs.social.network.common.security.JwtService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableDiscoveryClient
@EnableJpaRepositories
@SpringBootApplication
public class SocialNetworkAuthApplication {

  public static void main(String[] args) {
    SpringApplication.run(SocialNetworkAuthApplication.class, args);
  }

  @Bean
  public JwtService jwtService() {
    return new JwtService();
  }
}
