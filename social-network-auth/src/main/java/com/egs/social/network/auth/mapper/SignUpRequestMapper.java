package com.egs.social.network.auth.mapper;

import static com.egs.social.network.common.model.Role.ROLE_CLIENT;

import com.egs.social.network.auth.model.User;
import com.egs.social.network.common.dto.auth.SignUpRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.function.Function;
import org.springframework.stereotype.Component;

@Component
public final class SignUpRequestMapper implements Function<SignUpRequest, User> {
  /**
   * Maps {@link SignUpRequest} to {@link User}
   *
   * @param signUpRequest the entity
   * @return the User
   */
  @Override
  public User apply(SignUpRequest signUpRequest) {
    User user = new User();
    user.setEmail(signUpRequest.getEmail().toLowerCase(Locale.ENGLISH));
    user.setUsername(signUpRequest.getUserName().toLowerCase(Locale.ENGLISH));
    user.setPassword(signUpRequest.getPassword());
    user.setRoles(new ArrayList<>(Collections.singletonList(ROLE_CLIENT)));
    return user;
  }
}
