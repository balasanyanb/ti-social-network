package com.egs.social.network.auth.api;

import com.egs.social.network.auth.model.User;
import com.egs.social.network.auth.service.UserService;
import com.egs.social.network.common.dto.auth.SignInRequest;
import com.egs.social.network.common.dto.auth.SignInResponse;
import com.egs.social.network.common.dto.auth.SignUpRequest;
import com.egs.social.network.common.dto.auth.SignUpResponse;
import com.egs.social.network.common.security.JwtService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AuthControllerImpl implements AuthController {

  private final UserService userService;
  private final AuthenticationManager authenticationManager;
  private final JwtService jwtService;

  /**
   * API <host>:<port>/api/v1/auth/signup POST raw JSON { email: email@gmail.com, user_name: uname,
   * password : 12345678 }
   *
   * @param request the SignUpRequest for getting account details
   * @return ResponseEntity with HTTP Status code
   */
  @Override
  public ResponseEntity<SignUpResponse> signUp(@Valid @RequestBody SignUpRequest request) {
    log.info("Calling signUp for {} {}", request.getEmail(), request.getUserName());
    SignUpResponse response = userService.addUser(request);
    return ResponseEntity.status(HttpStatus.CREATED).body(response);
  }

  /**
   * API <host>:<port>/api/v1/auth/signin POST raw JSON { user_name: userName, password : 12345678 }
   *
   * @param request the SignInRequest for getting user by provided login and password
   * @return ResponseEntity with HTTP Status code
   */
  @Override
  public ResponseEntity<SignInResponse> signIn(@Valid @RequestBody SignInRequest request) {
    authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(request.getLogin(), request.getPassword()));
    User user = userService.findByEmailOrUserName(request.getLogin());
    String token = jwtService.generateToken(user.getRoles(), user.getExternalId());
    return ResponseEntity.ok(new SignInResponse(token));
  }
}
