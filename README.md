# Social Network

The Social Network aims are to propose a platform for communication with each other.

# Project presentation

The project is a multiple module maven projects.

* [social-network-auth](#social-network-auth)
* [social-network-common](#social-network-common)
* [social-network-messaging](#social-network-messaging)
* [social-network-eureka-server](#social-network-eureka-server)
* [social-network-account-management](#social-network-account-management)
* [social-network-zuul](#social-network-zuul)

#### social-network-auth
In this module placed global Security configuration related to project and everything related to user sign in, sign up. 

#### social-network-common
In this module placed all general classes which we need to use in all modules of our project like util classes, DTOs, custom exceptions, mappers, etc. 

#### social-network-messaging
In this module placed everything related to messaging between users.

#### social-network-eureka-server
In this module placed all configurations for Eureka Serve.

#### social-network-account-management
In this module placed everything related to a user account after user registered.

#### social-network-zuul
In this module placed all configurations for Zuul dynamic routing, monitoring, etc.

#### Last changes
Las changes on the project are 

- Added Dependency Management.
- Created JWTUtil for token reading and generation.  
- Creation of account when the user registered and didn’t have an account.  
- Added JWT token based Security for all microservices.  
- Save JWT token in ThreadLocal.
- Added ZUUL with it’s all functionality instead of manually, created api-gateway service.
- Completely refactored account microservice.  
- Added and edited several things in the common module like DTOs for the account, custom exceptions, mappers, etc.


## Todos
For the future we need to 
- Completely refactor social-network-messaging module
- Configure h2 or HSQLDB
- Add more unit and integration tests in all modules