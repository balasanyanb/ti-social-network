package com.egs.social.network.eureka.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class SocialNetworkEurekaServerApplication {

  public static void main(String[] args) {
    SpringApplication.run(SocialNetworkEurekaServerApplication.class, args);
  }
}
