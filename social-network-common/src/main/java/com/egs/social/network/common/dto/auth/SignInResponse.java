package com.egs.social.network.common.dto.auth;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignInResponse implements Serializable {

  private static final long serialVersionUID = -215509000454659840L;

  private String token;
}
