package com.egs.social.network.common.security;

import com.egs.social.network.common.util.ContextHolder;
import io.jsonwebtoken.Claims;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * JwtTokenAuthenticationFilter
 *
 * @author zaruhig
 */
@Component
public class JwtTokenAuthenticationFilter extends OncePerRequestFilter {

  private JwtService jwtService;

  public JwtTokenAuthenticationFilter(JwtService jwtService) {
    this.jwtService = jwtService;
  }

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain chain)
      throws ServletException, IOException {

    String token = jwtService.getTokenFromHeader(request);
    if (token != null) {

      Claims claims = jwtService.getAllClaimsFromToken(token);

      if (claims != null) {
        ContextHolder.setUserId(claims.get("externalId").toString());
        ContextHolder.setToken(token);

        @SuppressWarnings("unchecked")
        final List<String> authorities = (List<String>) claims.get("roles", List.class);

        UsernamePasswordAuthenticationToken auth =
            new UsernamePasswordAuthenticationToken(
                null,
                null,
                authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));

        SecurityContextHolder.getContext().setAuthentication(auth);
      }
    }
    try {

      chain.doFilter(request, response);

    } finally {
      ContextHolder.clear();
      SecurityContextHolder.clearContext();
    }
  }
}
