package com.egs.social.network.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
public class ValidationErrorResponseMy implements MySNError, Serializable {

  private static final long serialVersionUID = -4830891954415976756L;

  private int status;
  private List<ValidationError> errors;
}

