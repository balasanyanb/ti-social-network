package com.egs.social.network.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
class ValidationError implements Serializable {
  private static final long serialVersionUID = 3065931849408432008L;

  private String code;
  private String field;
  private String defaultMessage;
}
