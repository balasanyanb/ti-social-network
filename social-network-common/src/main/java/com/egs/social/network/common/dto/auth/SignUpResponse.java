package com.egs.social.network.common.dto.auth;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignUpResponse implements Serializable {

  private static final long serialVersionUID = -4437270152836645171L;

  private String message;
}
