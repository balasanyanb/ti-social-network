package com.egs.social.network.messaging.service;

import com.egs.social.network.messaging.dto.MessageResponse;
import com.egs.social.network.messaging.dto.SendMessageRequest;
import com.egs.social.network.messaging.model.Conversation;
import reactor.core.publisher.Flux;

public interface ConversationService {
  MessageResponse addMessage(SendMessageRequest request);

  Flux<Conversation> getUserConversations(long userId, int page);
}
