package com.egs.social.network.messaging.config;

import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;

@Configuration
@RequiredArgsConstructor
public class ReactiveWebSocketHandler {
  private final WebSocketHandler webSocketHandler;

  @Bean
  public HandlerMapping webSocketHandlerMapping() {
    Map<String, WebSocketHandler> urls = new HashMap<>();
    urls.put("/event-emitter", webSocketHandler);

    SimpleUrlHandlerMapping handlerMapping = new SimpleUrlHandlerMapping();
    handlerMapping.setOrder(1);
    handlerMapping.setUrlMap(urls);
    return handlerMapping;
  }

  /**
   * // @Bean // public WebSocketHandlerAdapter handlerAdapter() { // return new
   * WebSocketHandlerAdapter(); // }
   *
   * <p>// @Bean // HandlerMapping webSocketMapping(CommentService commentService, //
   * InboundChatService inboundChatService, // OutboundChatService outboundChatService) { //
   * Map<String, WebSocketHandler> urlMap = new HashMap<>(); // urlMap.put("/topic/comments.new",
   * commentService); // urlMap.put("/app/chatMessage.new", inboundChatService); //
   * urlMap.put("/topic/chatMessage.new", outboundChatService); // // SimpleUrlHandlerMapping
   * mapping = new SimpleUrlHandlerMapping(); // mapping.setOrder(10); // mapping.setUrlMap(urlMap);
   * // // return mapping; // }
   *
   * @return
   */
  @Bean
  WebSocketHandlerAdapter handlerAdapter() {
    return new WebSocketHandlerAdapter();
  }
}
