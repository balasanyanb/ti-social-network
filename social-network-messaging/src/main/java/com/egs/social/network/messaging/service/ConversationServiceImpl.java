package com.egs.social.network.messaging.service;

import com.datastax.driver.core.utils.UUIDs;
import com.egs.social.network.messaging.dto.MessageResponse;
import com.egs.social.network.messaging.dto.SendMessageRequest;
import com.egs.social.network.messaging.mapper.MessageMapper;
import com.egs.social.network.messaging.mapper.SendMessageRequestMapper;
import com.egs.social.network.messaging.model.Conversation;
import com.egs.social.network.messaging.model.Message;
import com.egs.social.network.messaging.repository.ReactiveConversationRepository;
import com.egs.social.network.messaging.repository.ReactiveMessageRepository;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@RequiredArgsConstructor
public class ConversationServiceImpl implements ConversationService {

  private final ReactiveMessageRepository messageRepository;
  private final ReactiveConversationRepository conversationRepository;

  @Override
  public MessageResponse addMessage(final SendMessageRequest request) {
    if (request.getConversationId() == null) {
      request.setConversationId(createConversation(request));
    }

    Message message = SendMessageRequestMapper.mapToMessage(request);
    message.setConversationId(request.getConversationId());
    Mono<Message> messageMono = messageRepository.save(message);
    messageMono.subscribe(m -> log.info(m.toString()), Throwable::printStackTrace);
    return MessageMapper.map(message);
  }

  private UUID createConversation(final SendMessageRequest request) {
    Conversation conversation = SendMessageRequestMapper.mapToConversation(request);
    UUID conversationId = UUIDs.timeBased();
    conversation.setConversationId(conversationId);
    conversationRepository.save(conversation).subscribe();
    conversation.setUserId(request.getToUser());
    conversationRepository.save(conversation).subscribe();
    return conversationId;
  }

  @Override
  public Flux<Conversation> getUserConversations(long userId, int page) {
    Flux<Conversation> conversations = conversationRepository.findByUserId(userId);
    long limit = 10;
    return conversations.skip(page * limit).take(limit);
  }
}
