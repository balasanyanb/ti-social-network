package com.egs.social.network.messaging.repository;

import com.egs.social.network.messaging.model.Conversation;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import reactor.core.publisher.Flux;

public interface ReactiveConversationRepository
    extends ReactiveCassandraRepository<Conversation, String> {
  Flux<Conversation> findByUserId(long userId);
}
