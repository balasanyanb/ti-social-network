package com.egs.social.network.messaging.model;

import com.datastax.driver.core.DataType;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table("messages")
public class Message1 {
  @Column(value = "to_user")
  private String toUser;

  @Column(value = "message_id")
  private UUID messageId;

  @Column private String message;

  @Column("from_user")
  private String fromUser;

  @Column(value = "created_at")
  private LocalDateTime createdAt;

  @CassandraType(type = DataType.Name.TIMEUUID)
  @PrimaryKey(value = "conversation_id")
  private UUID conversationId;

  @Column(value = "to_users")
  private Set<String> toUsers;
}
