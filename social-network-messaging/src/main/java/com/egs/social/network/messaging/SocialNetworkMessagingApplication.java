package com.egs.social.network.messaging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.cassandra.repository.config.EnableReactiveCassandraRepositories;

@SpringBootApplication
@EnableReactiveCassandraRepositories
public class SocialNetworkMessagingApplication {
  public static void main(String[] args) {
    SpringApplication.run(SocialNetworkMessagingApplication.class, args);
  }
}
