package com.egs.social.network.messaging.service;

public interface MessageService {
  void createConversation();

  void addMessage();
}
