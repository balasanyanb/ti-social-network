package com.egs.social.network.messaging.mapper;

import com.egs.social.network.messaging.dto.SendMessageRequest;
import com.egs.social.network.messaging.model.Conversation;
import com.egs.social.network.messaging.model.Message;
import java.time.LocalDateTime;
import java.util.UUID;

public final class SendMessageRequestMapper {

  private SendMessageRequestMapper() {
    // constructor
  }

  public static Conversation mapToConversation(SendMessageRequest request) {
    Conversation conversation = new Conversation();
    conversation.setUserId(request.getFromUser());
    conversation.setLabel(request.getFromUser() + "," + request.getToUser());
    conversation.setConversationId(request.getConversationId());
    conversation.setCreatedAt(LocalDateTime.now());
    return conversation;
  }

  public static Message mapToMessage(SendMessageRequest request) {
    Message message = new Message();
    message.setFromUser(request.getFromUser());
    message.setToUser(request.getToUser());
    message.setContent(request.getMessage());
    message.setMessageId(UUID.randomUUID());
    message.setCreatedAt(LocalDateTime.now());
    return message;
  }
}
