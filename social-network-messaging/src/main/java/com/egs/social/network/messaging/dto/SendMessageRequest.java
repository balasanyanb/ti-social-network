package com.egs.social.network.messaging.dto;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SendMessageRequest {
  private long toUser;
  private String message;
  private long fromUser;
  private UUID conversationId;
}
