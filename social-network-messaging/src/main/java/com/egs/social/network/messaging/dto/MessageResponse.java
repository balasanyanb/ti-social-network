package com.egs.social.network.messaging.dto;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageResponse {
  private long toUser;
  private long fromUser;
  private String message;
  private UUID messageId;
  private UUID conversationId;
  private LocalDateTime createdAt;
}
