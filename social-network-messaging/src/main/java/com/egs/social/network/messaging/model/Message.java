package com.egs.social.network.messaging.model;

import com.datastax.driver.core.DataType;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table("messages")
public class Message {
  @Column(value = "message_id")
  private UUID messageId;

  @Column(value = "to_user")
  private long toUser;

  @Column private String content;

  @Column("from_user")
  private long fromUser;

  @PrimaryKeyColumn(
      name = "created_at",
      ordinal = 1,
      type = PrimaryKeyType.CLUSTERED,
      ordering = Ordering.DESCENDING)
  private LocalDateTime createdAt;

  @CassandraType(type = DataType.Name.TIMEUUID)
  @PrimaryKeyColumn(name = "conversation_id", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
  private UUID conversationId;
}
