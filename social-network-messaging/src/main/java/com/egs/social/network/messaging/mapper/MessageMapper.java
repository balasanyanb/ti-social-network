package com.egs.social.network.messaging.mapper;

import com.egs.social.network.messaging.dto.MessageResponse;
import com.egs.social.network.messaging.model.Message;
import java.time.LocalDateTime;

public final class MessageMapper {

  private MessageMapper() {
  }

  public static MessageResponse map(Message message) {
    MessageResponse messageResponse = new MessageResponse();
    messageResponse.setToUser(message.getToUser());
    messageResponse.setMessage(message.getContent());
    messageResponse.setCreatedAt(LocalDateTime.now());
    messageResponse.setFromUser(message.getFromUser());
    messageResponse.setMessageId(message.getMessageId());
    messageResponse.setConversationId(message.getConversationId());
    return messageResponse;
  }
}
