package com.egs.social.network.messaging.repository;

import com.egs.social.network.messaging.model.Message;
import java.util.UUID;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;

public interface ReactiveMessageRepository extends ReactiveCassandraRepository<Message, UUID> {}
