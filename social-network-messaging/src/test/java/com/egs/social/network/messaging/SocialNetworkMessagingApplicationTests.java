package com.egs.social.network.messaging;

import static org.assertj.core.api.Java6Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SocialNetworkMessagingApplicationTests {

  @Test
  public void contextLoads() {
    assertThat(true).isTrue();
  }
}
