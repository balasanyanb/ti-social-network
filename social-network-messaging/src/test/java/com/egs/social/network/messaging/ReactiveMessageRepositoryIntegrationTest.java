package com.egs.social.network.messaging;

import com.datastax.driver.core.utils.UUIDs;
import com.egs.social.network.messaging.model.Message;
import com.egs.social.network.messaging.repository.ReactiveMessageRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ReactiveMessageRepositoryIntegrationTest {

    private final int limit = 5;
    private int toUser = 10;
    private int fromUser = 8;
    private String content = "Hi";
    private UUID uuid = UUIDs.timeBased();

    @Autowired
    private ReactiveMessageRepository repository;

    @Test
    public void testAddMessage() {
        Message message = new Message();
        message.setFromUser(fromUser);
        message.setToUser(toUser);
        message.setContent(content);
        message.setMessageId(UUID.randomUUID());
        message.setCreatedAt(LocalDateTime.now());
        message.setConversationId(uuid);
        Mono<Message> mono = repository.save(message);
        assertNotNull((mono.block()));
        mono.subscribe((message1 -> {
            log.info(message1.toString());
        }));
    }

    @Test
    public void testGetUserMessages() {
        Flux<Message> messages = repository.findAllById(Flux.just(uuid));
        int page = 0;
        assertNotNull(messages.skip(page * limit).take(limit));
    }

    @Test
    public void testGetAll() {
        Flux<Message> messages = repository.findAll();
        assertNotNull(messages.blockFirst());
        printMessages(messages);
    }

    private void printMessages(Flux<Message> messages) {
        messages.subscribe((message) -> {
            log.info(message.toString());
        }, Throwable::printStackTrace);
    }
}
