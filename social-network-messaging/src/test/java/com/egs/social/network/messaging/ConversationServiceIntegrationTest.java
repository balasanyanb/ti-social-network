package com.egs.social.network.messaging;

import static org.assertj.core.api.Assertions.assertThat;

import com.egs.social.network.messaging.dto.MessageResponse;
import com.egs.social.network.messaging.dto.SendMessageRequest;
import com.egs.social.network.messaging.model.Conversation;
import com.egs.social.network.messaging.service.ConversationService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ConversationServiceIntegrationTest {
  @Autowired private ConversationService conversationService;

  @Test
  public void testAddNewMessage() {
    SendMessageRequest request = new SendMessageRequest();
    request.setFromUser(1);
    request.setToUser(2);
    request.setMessage("hi 2");
    MessageResponse response = conversationService.addMessage(request);
    log.info(response.toString());

    assertThat(true).isTrue();
  }

  @Test
  public void testReply() {
    SendMessageRequest request = new SendMessageRequest();
    request.setFromUser(2);
    request.setToUser(1);
    request.setMessage("hi 1");
    //        request.setConversationId();
    conversationService.addMessage(request);

    assertThat(true).isTrue();
  }

  @Test
  public void testGetUserConversations() {
    Flux<Conversation> conversations = conversationService.getUserConversations(1, 0);
    //        assertNotNull((conversations.blockFirst()));
    conversations.subscribe(
        (conversation) -> {
          log.info(conversation.toString());
        },
        Throwable::printStackTrace);

    assertThat(true).isTrue();
  }
}
