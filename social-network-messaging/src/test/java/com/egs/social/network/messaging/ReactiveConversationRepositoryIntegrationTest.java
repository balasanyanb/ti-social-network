package com.egs.social.network.messaging;

import com.datastax.driver.core.utils.UUIDs;
import com.egs.social.network.messaging.model.Conversation;
import com.egs.social.network.messaging.repository.ReactiveConversationRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ReactiveConversationRepositoryIntegrationTest {
    private final int limit = 5;
    private int toUser = 4;
    private int fromUser = 8;
    @Autowired
    private ReactiveConversationRepository repository;

    @Test
    public void testCreateConversation() {
        Conversation conversation = new Conversation();
        conversation.setUserId(fromUser);
        conversation.setLabel(fromUser + "-" + toUser);
        conversation.setConversationId(UUIDs.timeBased());
        conversation.setCreatedAt(LocalDateTime.now());
        Mono<Conversation> saved = repository.save(conversation);
        assertNotNull((saved.block()));

        conversation.setUserId(toUser);
        Mono<Conversation> duplicate = repository.save(conversation);
        assertEquals(duplicate.block().getUserId(), conversation.getUserId());
    }

    @Test
    public void testGetUserConversations() {
        Flux<Conversation> conversations = repository.findByUserId(fromUser);
        int page = 0;
        assertNotNull(conversations.skip(page * limit).take(limit));
    }

    @Test
    public void testGetAll() {
        Flux<Conversation> conversations = repository.findAll();
        assertNotNull(conversations.blockFirst());
        printConversations(conversations);
    }

    private void printConversations(Flux<Conversation> conversations) {
        conversations.subscribe((conversation) -> {
            log.info(conversation.toString());
        }, Throwable::printStackTrace);
    }
}
